package virtualMachine.stackArithmetic

fun add(a: Int, b: Int) : Int = a + b

fun sub(a: Int, b: Int) : Int = a - b

fun neg(a: Int) : Int = -a