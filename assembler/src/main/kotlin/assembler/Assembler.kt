package assembler

import padTo16

class Assembler(private val aInstruction: AInstruction, private val cInstruction: CInstruction,
                private val labelVariables: LabelVariables) {

    /**
     * Takes standard input line e.g. M=1 and writes out that in 16 bit binary string
     */
    fun assembleLine(line: String): String {
        val instruction = sanitseInputs(line.trim())
        val instruction16Bit: String = if (instruction.contains("@")) {
            if (labelVariables.exposeVariables().containsKey(instruction)) {
                val memLoc: Int? = labelVariables.exposeVariables()[instruction]
                padTo16(Integer.toBinaryString(memLoc!!))
            } else {
                aInstruction.processAInstruction(instruction)
            }
        } else {
            cInstruction.processCInstruction(instruction)
        }

        if (instruction16Bit.length == 16) {
            return insertSpaceEvery4Characters(instruction16Bit)
        } else {
            throw AssemblyProcessingException("Invalid input detected\"$instruction\"")
        }

    }

    fun sanitseInputs(trimmedLine: String): String {
        return if (trimmedLine.contains("//")) {
            val noComments = trimmedLine.substring(0, trimmedLine.indexOf("//"))
            return noComments.trim()
        } else {
            trimmedLine
        }
    }

    private fun insertSpaceEvery4Characters(line: String) : String {
        return line.substring(0, 4) + " " + line.substring(4,8) + " " + line.substring(8,12) + " " + line.substring(12)
    }

    class AssemblyProcessingException(message: String) : Exception(message)

}