// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux.hdl

/**
 * Demultiplexor:
 * {a, b} = {in, 0} if sel == 0
 *          {0, in} if sel == 1
 */

CHIP DMux {
    IN in, sel;
    OUT a, b;

    /**
    Takes a single input and a selector and channels it to one of 2 possible outcomes based on a 
    selector bit. If in is 0, then a,b are zero but if it is 1 and equal to sel then out is 1
    */
    PARTS:
    Not(in=sel, out=notSel);
    Mux(a=false, b=in, sel=notSel, out=a);
    Mux(a=false, b=in, sel=sel, out=b);
}
