package IO

import assembler.AInstruction
import assembler.Assembler
import assembler.CInstruction
import assembler.LabelVariables
import java.io.File
import java.nio.charset.Charset
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import kotlin.streams.toList

fun preformAssembly(pathToFile: String): List<String> {
    val file = File(pathToFile)
    val bufferedReader = file.bufferedReader(Charset.defaultCharset())
    val assemblyCode: List<String> = bufferedReader
            .lines()
            .filter { line -> !line.startsWith("//") && !line.isEmpty() }
            .toList()

    val labelVariables = LabelVariables()
    labelVariables.initialize(assemblyCode) //should probs be in constructor
    val assembler = Assembler(AInstruction(), CInstruction(), labelVariables)
    return assemblyCode
            .filter {
                //do not process comments or (LOOP)s
                it ->
                !(it.startsWith("//") || it.startsWith('(') || it.isEmpty())
            }
            .map { it ->
                assembler.assembleLine(it)
            }
}

fun writeStringToFile(contents: String, fileLoc: String) = File(fileLoc).writeText(contents, Charset.defaultCharset())

fun writeListToFile(contents: List<String>, fileLoc: String) {
    val pathToOutPutFile: Path = FileSystems.getDefault().getPath(fileLoc)
    Files.write(pathToOutPutFile, contents)
}

