// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

//for i in range(R1)
//  R2 += R2 
	@sum //sum refers to a memory location 
	M=0 
    @i 
    M=1
(LOOP)
    @i
    D=M //loads data stored at mem address i into D register 
    @R1
    D=D-A //D = i - val R1 
    @END 
    D;JGT //if D zero jump to end 
    @R2
    D=M 
    @sum 
    M=D+M //incremement sum by R2 
    @i 
    M=M+1 
    @LOOP 
    0;JMP
(END) 	//creates infinite loop-ends program 
    @END 
    0;JMP
