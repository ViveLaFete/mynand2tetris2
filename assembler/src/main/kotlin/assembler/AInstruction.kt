package assembler

import isStringNumber
import padTo16

class AInstruction {

    //variables are mapped to successive memory locations as they are encountered starting at 16
    private val knownVariables: MutableMap<String, Int> = HashMap()
    private val MIN_MEMORY_LOCATION = 16

    fun processAInstruction(line: String): String {
        //is a symbol
        var binaryString = ""
        if (line.contains("@")) {
            val value: String = line.substring(line.indexOf('@') + 1)
            if (!value.isEmpty()) {
                binaryString = if (isStringNumber(value)) {
                    //-ve numbers?
                    padTo16(Integer.toBinaryString(value.toInt()))
                } else {
                    processVariable(value)
                }
            }
        }
        return binaryString
    }

    private fun processVariable(variable: String): String {
        return when {
            knownVariables.isEmpty() -> {
                setFirstVariable(variable)
            }
            knownVariables.containsKey(variable) -> {
                getExistingVariable(variable)
            }
            else -> {
                insertVariableInNextAvailableAddress(variable)
            }
        }
    }

    private fun insertVariableInNextAvailableAddress(variable: String): String {
        val maxMemoryLocation: Int = knownVariables.values.max()!!
        knownVariables[variable] = maxMemoryLocation + 1
        return padTo16(Integer.toBinaryString((maxMemoryLocation + 1)))
    }

    private fun getExistingVariable(variable: String): String {
        val memoryLocation: Int? = knownVariables[variable]
        return padTo16(Integer.toBinaryString(memoryLocation!!))
    }

    private fun setFirstVariable(variable: String): String {
        knownVariables[variable] = MIN_MEMORY_LOCATION
        return padTo16(Integer.toBinaryString(MIN_MEMORY_LOCATION))
    }
}