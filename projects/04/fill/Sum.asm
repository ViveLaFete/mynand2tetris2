//computes sum=1+...+100 
	@i //i refers to some memory location 
	M=1 //set i to 1 
	@sum //sum refers to a memory location 
	M=0 //set M to 0
(LOOP)
	@i	//if (1 - 100) = 0 goto END 
	D=M  //D = data register, store value of i in D 
	@100
	D=D-A //A = 100, then store res of i - A in D 
	@END 
	D;JGT
	@i    // sum += 1
	D=M
	@sum
	M=D+M
	@i
	D=M
	@sum
	M=D+M 
	@i  // i++ 
	M=M+1
	@LOOP //goto LOOP 
	0;JMP 
(END) 	//creates infinite loop-ends program 
	@END 
	0;JMP 
	