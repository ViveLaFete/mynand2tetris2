package virtualMachine.stackArithmetic

fun eq(a: Int, b: Int) : Boolean = a == b

fun eq(a: Boolean, b: Boolean) : Boolean = a == b

fun lt(a: Int, b: Int) : Boolean = a < b

fun gt(a: Int, b: Int) : Boolean = a > b

fun and(a: Boolean, b: Boolean) : Boolean = a && b

fun or(a: Boolean, b: Boolean) : Boolean = a || b

fun not(a: Boolean) = !a